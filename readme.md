## DataReader for Nuke by Hendrik Proosa

Example custom format reader, reads ASCII files, can be used for reading LIDAR etc data.


Current version does not have different format options and is specifically for reading space separated XYZIRGB data.
This can easily be changed in code by changing the stride, color and position offsets.
Customizing the number separator is a bit more complicated at the moment.

Reading is single-threaded but still relatively fast, I can read a 250MB test file which contains 6 million points in about 2.5 seconds.

Example of data that current version reads:
```
-0.182903 -0.006469 -0.000073 -1016 83 151 200
-0.205165 -0.277344 0.030627 -455 28 46 50
-0.205094 -0.277209 0.031238 177 46 61 64
-0.207391 -0.310896 0.030933 288 39 57 77
-0.207505 -0.310389 0.030383 375 39 57 77
-0.207173 -0.325028 0.031879 -1058 35 52 60
-0.207174 -0.310218 0.030841 376 39 57 77
-0.207191 -0.309658 0.030780 413 39 57 77
-0.205413 -0.309486 0.027789 -1077 27 59 70
-0.204457 -0.309435 0.027820 -1143 27 59 70
-0.205982 -0.308465 0.027820 -1015 27 59 70
-0.207805 -0.315571 0.030688 -853 37 59 70
-0.206722 -0.313830 0.030139 -963 37 59 70
-0.206929 -0.313239 0.030139 -945 37 59 70
-0.207470 -0.314471 0.030688 -882 37 59 70
```
